//package com.dadada.AutoPark;
//
//import com.zaxxer.hikari.HikariConfig;
//import com.zaxxer.hikari.HikariDataSource;
//import com.zaxxer.hikari.HikariPoolMXBean;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//import org.springframework.orm.jpa.JpaTransactionManager;
//import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
//import org.springframework.transaction.PlatformTransactionManager;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//
//import javax.persistence.EntityManagerFactory;
//import javax.sql.DataSource;
//import java.sql.Connection;
//import java.sql.SQLException;
//
//    @Configuration
//    @EnableTransactionManagement
//    @EnableJpaRepositories(
//            entityManagerFactoryRef = "entityManagerFactory",
//            transactionManagerRef = "transactionManager",
//            basePackages = "ct.oms.persistence")
//    public class AutoParkDataSource {
//
//        private final PoolConfigurator connectionSettings;
//
//
//        @Autowired
//        public AutoParkDataSource(PoolConfigurator connectionSettings) {
//            this.connectionSettings = connectionSettings;
//        }
//
//        @Bean(name = "dataSource")
//        @Qualifier("oms")
//
//        public HikariDataSource oms() {
//            HikariConfig hikariConfig = new HikariConfig();
//            hikariConfig.setDriverClassName(connectionSettings.getJdbcDriver());
//            hikariConfig.setJdbcUrl(connectionSettings.getJdbcString());
//            hikariConfig.setUsername(connectionSettings.getJdbcUser());
//            hikariConfig.setPassword(connectionSettings.getJdbcPassword());
//            hikariConfig.setMaximumPoolSize(connectionSettings.getJdbcMaxPoolSize());
////        hikariConfig.setConnectionTestQuery(connectionSettings.getJdbcConnectionTestQuery());
//            hikariConfig.setConnectionTestQuery("select 'oms' from dual");
//            hikariConfig.setValidationTimeout(30000);
//
//            //hikariConfig.setRegisterMbeans(true); // чтобы пул можно было мониторить
//
//            hikariConfig.setPoolName("omsPool");
//            return new HikariDataSource(hikariConfig);
//        }
//
//
//        @Bean(name = "entityManagerFactory")
//        public LocalContainerEntityManagerFactoryBean
//        entityManagerFactory(
//                EntityManagerFactoryBuilder builder,
//                @Qualifier("dataSource") DataSource dataSource
//        ) {
//            return builder
//                    .dataSource(dataSource)
//                    .packages("ct.oms.model")
//                    .persistenceUnit("ct.oms")
//                    .build();
//        }
//
//
//        @Bean(name = "transactionManager")
//        public PlatformTransactionManager transactionManager(
//                @Qualifier("entityManagerFactory") EntityManagerFactory
//                        entityManagerFactory
//        ) {
//            return new JpaTransactionManager(entityManagerFactory);
//        }
//
//        @Bean("getOmsPool")
//        @Qualifier("getOmsHikariMxBean")
//        public HikariPoolMXBean getOmsHikariMxBean(@Qualifier("dataSource") HikariDataSource dataSource) {
//            return dataSource.getHikariPoolMXBean();
//        }
//
//        @Bean(name = "omsConnection")
//        public Connection getConnection(@Qualifier("dataSource") DataSource dataSource) {
//            try {
//                return dataSource.getConnection();
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
//            return null;
//        }
//}
//
