package com.AutoParkProject.AutoPark.Controllers;

import com.AutoParkProject.AutoPark.Repos.EmployeeRepository;
import com.AutoParkProject.AutoPark.Repos.RoutesRepository;
import com.AutoParkProject.AutoPark.clases.Employee;
import com.AutoParkProject.AutoPark.clases.Routes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins ="*")
@RequestMapping("/routes")
public class RoutesController {
    @Autowired
    private RoutesRepository routesRepository;

    @RequestMapping("/getAll")
    public Routes[] getAll() {
        return routesRepository.getAll();

    }
}
