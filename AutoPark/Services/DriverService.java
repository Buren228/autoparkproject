//package com.AutoParkProject.AutoPark.Services;
//
//import com.dadada.AutoPark.Repos.DriverRepository;
//import com.dadada.AutoPark.clases.Driver;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//
//@Service
//public class DriverService {
//    @Autowired
//    private DriverRepository repo;
//
//    public List<Driver> listAll()
//    {
//        return  repo.findAll();
//    }
//
//    public void save(Driver driver)
//    {
//        repo.save(driver);
//    }
//    public Driver get(Long id)
//    {
//        return  repo.findById(id).get();
//    }
//    public void delete(Long id)
//    {
//        repo.deleteById(id);
//    }
//}
