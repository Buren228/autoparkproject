//package com.AutoParkProject.AutoPark.Services;
//
//import com.dadada.AutoPark.Repos.DriverBusRepository;
//import com.dadada.AutoPark.clases.DriverBus;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//@Service
//public class DriverBusService {
//    @Autowired
//    private DriverBusRepository repo;
//
//    public List<DriverBus> listAll()
//    {
//        return  repo.findAll();
//    }
//
//    public void save(DriverBus driverBus)
//    {
//        repo.save(driverBus);
//    }
//    public DriverBus get(Long id)
//    {
//        return  repo.findById(id).get();
//    }
//    public void delete(Long id)
//    {
//        repo.deleteById(id);
//    }
//}
//
