export class Buses {


  constructor(public bus_id?:number,
              public number?,
              public mark?,
              public release_date?,
              public status?,
              public routeID?,
              ) {

  }
}
