export enum Roles{
  ADMIN="admin",
  DISPATCHER="dispatcher",
  DRIVER="driver",
  CONTROLLER="controller"
}
